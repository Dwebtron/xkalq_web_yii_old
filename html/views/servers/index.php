<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Servers');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        All servers use 'pandapanda' as the "shared secret".
        <?php // Html::a(Yii::t('app', 'Create Servers'), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'',
     //   'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

          //  'indexx',
            'city',
            'url1',
          //  'url2',
          //  'url3',
            'ip1',
            [
                'label' => 'Shared Secret',
                'value' => function () { return 'pandapanda'; } ,
            ],
          //  'ip2',
            //'ip3',
            //'host',
            //'hostUrl',
            //'created',
            //'updated',
            //['class' => 'yii\grid\ActionColumn'],


        ],
    ]); ?>
</div>
