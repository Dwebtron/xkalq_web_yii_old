<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ServersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'indexx') ?>

    <?= $form->field($model, 'city') ?>

    <?= $form->field($model, 'url1') ?>

    <?= $form->field($model, 'url2') ?>

    <?= $form->field($model, 'url3') ?>

    <?php // echo $form->field($model, 'ip1') ?>

    <?php // echo $form->field($model, 'ip2') ?>

    <?php // echo $form->field($model, 'ip3') ?>

    <?php // echo $form->field($model, 'host') ?>

    <?php // echo $form->field($model, 'hostUrl') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
