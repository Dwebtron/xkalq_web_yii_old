<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Servers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hostUrl')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
