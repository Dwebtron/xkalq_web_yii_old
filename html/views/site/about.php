<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This project was made with love split between New Jersey, Herat, and Shanghai. We may not be as big or robust as our competitors, but we make up for that with focus. We are laser-focused on delivering censorship circumvention technologies for those living in oppressed places to access information. We strongly believe in free access to information.

        It's for this reason that we do everything we can to try to ensure that we have created the most secure tool possible to allow you to freely and quickly access important tools that are blocked in your region.
    </p>

    <!--<code><?php //__FILE__ ?></code> -->
</div>
