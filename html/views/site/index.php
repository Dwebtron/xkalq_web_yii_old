<?php

/* @var $this yii\web\View */

$this->title = 'Xkalq VPN';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Xkalq VPN</h1>

        <p class="lead">Allowing Freedom of Speech and Thought.</p>
        <p><a class="btn btn-lg btn-success" href="">Beta Sign-Up</a></p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>Android</h2>

                <p>The Android application is in Beta! Click <a href=""> here for the documentation</a></p>
            </div>
            <div class="col-lg-3">
                <h2>iOS Devices</h2>

                <p>At this time, iOS devices do not have a dedicated application from us. However, they are still compatible. Click <a href=""> here for the documentation</a> </p>

            </div>
            <div class="col-lg-3">
                <h2>Windows</h2>

                <p>At this time, Windows devices do not have a dedicated application from us. However, they are still compatible. Click <a href=""> here for the documentation</a> </p>

            </div>
            <div class="col-lg-3">
                <h2>MacOS</h2>

                <p>At this time, Macs do not have a dedicated application from us. However, they are still compatible. Click <a href=""> here for the documentation</a> </p>

            </div>
        </div>

    </div>
</div>
