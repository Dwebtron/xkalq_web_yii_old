<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\db\Expression;
/**
 * This is the model class for table "servers".
 *
 * @property int $indexx
 * @property string $city the city that the server is in
 * @property string $url1 the server's current URL
 * @property string $url2 the server's current URL
 * @property string $url3 the server's current URL
 * @property string $ip1 the server's current IP
 * @property string $ip2 the server's current IP
 * @property string $ip3 the server's current IP
 * @property string $host which service the server is hosted on
 * @property string $hostUrl URL of the server host
 * @property string $created
 * @property string $updated
 */
class Servers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'url1', 'url2', 'url3', 'ip1', 'ip2', 'ip3', 'host', 'hostUrl', 'created'], 'safe'],
            [['created', 'updated'], 'safe'],
            [['city', 'url1', 'url2', 'url3', 'ip1', 'host', 'hostUrl'], 'string', 'max' => 255],
            [['ip2', 'ip3'], 'string', 'max' => 252],
        ];
    }

    public function behaviors()
    {
        return [
            // Other behaviors
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'city' => Yii::t('app', 'City'),
            'url1' => Yii::t('app', 'URL'),
            'url2' => Yii::t('app', 'the server\'s current URL'),
            'url3' => Yii::t('app', 'the server\'s current URL'),
            'ip1' => Yii::t('app', 'IP'),
            'ip2' => Yii::t('app', 'the server\'s current IP'),
            'ip3' => Yii::t('app', 'the server\'s current IP'),
            'host' => Yii::t('app', 'which service the server is hosted on'),
            'hostUrl' => Yii::t('app', 'URL of the server host'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }



    /**
     * {@inheritdoc}
     * @return ServersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServersQuery(get_called_class());
    }
}
