<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Servers;

/**
 * ServersSearch represents the model behind the search form of `app\models\Servers`.
 */
class ServersSearch extends Servers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['indexx'], 'integer'],
            [['city', 'url1', 'url2', 'url3', 'ip1', 'ip2', 'ip3', 'host', 'hostUrl', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'indexx' => $this->indexx,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'url1', $this->url1])
            ->andFilterWhere(['like', 'url2', $this->url2])
            ->andFilterWhere(['like', 'url3', $this->url3])
            ->andFilterWhere(['like', 'ip1', $this->ip1])
            ->andFilterWhere(['like', 'ip2', $this->ip2])
            ->andFilterWhere(['like', 'ip3', $this->ip3])
            ->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'hostUrl', $this->hostUrl]);

        return $dataProvider;
    }
}
